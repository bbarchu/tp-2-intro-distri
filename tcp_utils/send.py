import struct
import os
from .constants import METADATA_LENGTH_SIZE, OP_MODE_SIZE
from .connection_closed_error import ConnectionClosedError

def send_data(conn, data, size):
  bytes_sent = 0
  while bytes_sent < size:
    sent = conn.send(data)
    if sent == 0:
      raise ConnectionClosedError
    bytes_sent += sent
    data = data[bytes_sent:]

def send_file_name(conn, file_name):
  name_length = struct.pack('<I', len(file_name))
  send_data(conn, name_length, METADATA_LENGTH_SIZE)
  send_data(conn, file_name.encode(), len(file_name))

def send_operating_mode(conn, op_mode):
  send_data(conn, op_mode, OP_MODE_SIZE)

def send_file_length(conn, file_path):
  file_length = os.path.getsize(f'{file_path}')
  length = struct.pack('<I', file_length)
  send_data(conn, length, METADATA_LENGTH_SIZE)