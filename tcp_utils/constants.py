CHUNK_SIZE = 4096
OP_CODE_UPLOAD = b'UPLD'
OP_CODE_DOWNLOAD = b'DNLD'
METADATA_LENGTH_SIZE = 4
OP_MODE_SIZE = 4
SERVER_CONFIRMATION_LENGTH = 3
ERROR_RESPONSE = b'ERR'
ACK_RESPONSE = b'ACK'