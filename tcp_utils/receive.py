import struct
from .constants import METADATA_LENGTH_SIZE
from .connection_closed_error import ConnectionClosedError

def recv_data(conn, size, max_chunk_size = -1):
  max_chunk_size = size if max_chunk_size == -1 else max_chunk_size
  bytes_read = 0
  buf = bytes()
  while bytes_read < size and bytes_read < max_chunk_size:
    data = conn.recv(max_chunk_size)
    if data == b'':
      raise ConnectionClosedError
    bytes_read += len(data)
    buf += data
  return buf[:size]

def recv_file_length(conn):
  data = recv_data(conn, METADATA_LENGTH_SIZE)
  file_length, = struct.unpack('<I', data)
  return file_length

def recv_file_name(conn):
  data = recv_data(conn, METADATA_LENGTH_SIZE)
  name_length, = struct.unpack('<I', data)
  data = recv_data(conn, name_length)
  return data.decode()