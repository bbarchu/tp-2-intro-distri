import socket
import struct
import os
from tcp_utils.receive import recv_data, recv_file_length, recv_file_name
from tcp_utils.send import send_data, send_file_length
from tcp_utils.constants import OP_MODE_SIZE, CHUNK_SIZE, ERROR_RESPONSE, ACK_RESPONSE
from tcp_utils.connection_closed_error import ConnectionClosedError

def start_server(server_address, storage_dir):
  if not os.path.isdir(storage_dir):
    print(f"Error starting the server: {storage_dir} is not a directory")
    return

  print('TCP: start_server({}, {})'.format(server_address, storage_dir))
  serv = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
  serv.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
  serv.bind(server_address)
  serv.listen(10)

  while True:
    try:
      conn, address = serv.accept()
      print(f'Connection started with address {address}')
      operating_mode = recv_data(conn, OP_MODE_SIZE)
      if operating_mode == b'UPLD':
        _upload(conn, storage_dir)
      elif operating_mode == b'DNLD':
        _download(conn, storage_dir)
      else:
        print(f'Operation mode {operating_mode} not supported')
        break
    except OSError:
      print('The client connection was closed unexpectedly')
    finally:
      conn.close()

  serv.close()

def _upload(conn, storage_dir):
  try:
    file_name = recv_file_name(conn)
    file_length = recv_file_length(conn)
    file_path = f'{storage_dir}/{file_name}'
    
    with open(file_path, 'wb') as file:
      bytes_read = 0
      while bytes_read < file_length:
        data = recv_data(conn, file_length - bytes_read, CHUNK_SIZE)
        bytes_read += len(data)
        file.write(data)
  except ConnectionClosedError:
    print(f'The client connection was closed unexpectedly')
    if os.path.isfile(file_path):
      os.remove(file_path)
  except OSError:
    print(f'Error could not write to file {file_name}')

def _download(conn, storage_dir):
  file_name = recv_file_name(conn)
  file_path = f'{storage_dir}/{file_name}'
  
  print(f'Request to download file {file_path}')
  if not os.path.isfile(f'{storage_dir}/{file_name}'):
    send_data(conn, ERROR_RESPONSE, len(ERROR_RESPONSE))
    return
  else:
    send_data(conn, ACK_RESPONSE, len(ACK_RESPONSE))
  
  send_file_length(conn, file_path)
  try:
    with open(f'{file_path}', 'rb') as file:
      data = file.read(CHUNK_SIZE)
      while data != b'':
        send_data(conn, data, len(data))
        data = file.read(CHUNK_SIZE)
  except OSError:
    print(f'Could not read from file {file_path}')

