import socket
import struct
import os
from datetime import datetime

CHUNK_SIZE = 32
PAYLOAD_SIZE = CHUNK_SIZE - 10
CONNECTION_TIMEOUT_SECS = 60

class UdpServer():
    def __init__(self):
        self.last_sequence_number = 0
        self.sequence_number = 1
        self.sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        self.file_length = 0
        self.file_name = ''
                

    def start_server(self, server_address, storage_dir):
        print('UDP: start_server({}, {})'.format(server_address, storage_dir))
        self.storage_dir = storage_dir
        self.sock.bind(server_address)

        while True:
            self.last_sequence_number = 0
            self.sequence_number = 1
            operating_mode, addr = self._recv_operation_mode()
            self.remote_address = addr
            if operating_mode == b'UPLD':
              self._upload()
            elif operating_mode == b'DNLD':
              self._download()
            else:
              print(f'Operation mode {operating_mode} not supported')
        self.sock.close()
    
    def _upload(self):
        self.file_name = self._recv_file_name()
        print('Uploading file {}'.format(self.file_name))
        self.file_length = self._recv_file_length()

        with open(f'{self.storage_dir}/{self.file_name}', 'wb') as file:
            bytes_read = 0
            while bytes_read < self.file_length:
                data, addr = self._recv_data()
                bytes_read += len(data)
                file.write(data)
    
    def _download(self):
        self.file_name = self._recv_file_name()
        print('Retrieving file {}'.format(self.file_name))
        if not os.path.isfile(f'{self.storage_dir}/{self.file_name}'):
            print('Error: inexistent file')
            self.ensure_send(b'ERR')
            return
        
        self.ensure_send(b'ACK')
        self._send_file_length(f'{self.storage_dir}/{self.file_name}')

        with open(f'{self.storage_dir}/{self.file_name}', 'rb') as file:
            data = file.read(PAYLOAD_SIZE)
            while data:
                self.ensure_send(data)
                data = file.read(PAYLOAD_SIZE)

    def _recv_operation_mode(self):
        data, addr = self._recv_data()
        return (data, addr)

    def _recv_file_name(self):
        data, addr = self._recv_data()
        name_length, = struct.unpack('<I', data)
        data, addr = self._recv_data()
        return data.decode()
    
    def _recv_file_length(self):
        data, addr = self._recv_data()
        file_length, = struct.unpack('<I', data)
        return file_length

    def _recv_data(self):
        while True:
            data, addr = self.sock.recvfrom(CHUNK_SIZE)
            sequence_number = data[:10]
            data = data[10:]
            self.sock.sendto(sequence_number, addr)

            if (int(sequence_number) == (self.last_sequence_number + 1)):
                self.last_sequence_number +=1
                return data, addr

    def _recv_response(self):
        try:
            signal, addr = self.sock.recvfrom(CHUNK_SIZE)
            while int(signal.decode()) != self.sequence_number:
                signal, addr = self.sock.recvfrom(CHUNK_SIZE)
            return True
        except (socket.timeout):
            return False

    def ensure_send(self, data):
        start = datetime.now()
        if(self.sequence_number > 9999999999):
            self.sequence_number=1
        self._send_data(data)
        while(not self._recv_response()):
            now = datetime.now()
            if (now - start).seconds >= CONNECTION_TIMEOUT_SECS:
              raise socket.timeout
            self._send_data(data)
        self.sequence_number +=1
    
    def _send_data(self,data):   
        data = str(self.sequence_number).zfill(10).encode() + data
        self.sock.sendto(data,self.remote_address)
    
    def _send_file_length(self,src):
        file_length = os.path.getsize(src)
        length = struct.pack('<I', file_length)
        self.ensure_send(length)
