import socket
import os
import struct
from datetime import datetime

CHUNK_SIZE = 32
PAYLOAD_SIZE = CHUNK_SIZE - 10

class UDPUploader():
    def __init__(self):
        self.sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        self.sock.settimeout(1)
        self.sequence_number = 1
        self.server_address = None

    def upload_file(self,server_address, src, name):
        print('UDP: upload_file({}, {}, {})'.format(server_address, src, name))
        if not os.path.isfile(src):
            print('Error: file does not exist')
            return
        self.server_address = server_address
        self._send_operation_mode_upload()
        self._send_file_name(name)
        self._send_file_length(src)
        
        with open(src, 'rb') as file:
            data = file.read(PAYLOAD_SIZE)
            while data:
                self.ensure_send(data)
                data = file.read(PAYLOAD_SIZE )

        file.close()
        self.sock.close()

    def ensure_send(self, data):
        start = datetime.now()
        if(self.sequence_number > 9999999999):
            self.sequence_number=1
        self._send_data(data)
        while(not self._recv_server_response()):
            now = datetime.now()
            if (now - start).seconds >= CONNECTION_TIMEOUT_SECS:
              raise socket.timeout
            self._send_data(data)
        self.sequence_number +=1


    def _recv_server_response(self):
        try:
            signal, addr = self.sock.recvfrom(CHUNK_SIZE)
            while int(signal.decode()) != self.sequence_number:
                signal, addr = self.sock.recvfrom(CHUNK_SIZE)
            return True
        except (socket.timeout):
            return False
   
    def _send_data(self,data):
       
        data = str(self.sequence_number).zfill(10).encode() + data
        self.sock.sendto(data,self.server_address)

    def _send_file_name(self,file_name):
        name_length = struct.pack('<I', len(file_name))
        self.ensure_send(name_length)
        self.ensure_send(file_name.encode())


    def _send_file_length(self,src):
        file_length = os.path.getsize(src)
        length = struct.pack('<I', file_length)
        self.ensure_send(length)
    
    def _send_operation_mode_upload(self):
        self.ensure_send(b'UPLD')