import socket
import os
import struct
from datetime import datetime

CHUNK_SIZE = 32
PAYLOAD_SIZE = CHUNK_SIZE - 10
CONNECTION_TIMEOUT_SECS = 60

class UDPDownloader():
    def __init__(self):
        self.sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        self.sock.settimeout(1)
        self.sequence_number = 1
        self.last_sequence_number = 0
        self.server_address = None

    def download_file(self,server_address, dst, name):
        print('UDP: download_file({}, {}, {})'.format(server_address, dst, name))
        try:
          self.server_address = server_address
          self._send_operation_mode_download()
          self._send_file_name(name)
          response, addr = self._recv_data()
          if response == b'ERR':
            print('File does not exist on the server')
            self.sock.close()
            return

          self.file_length = self._recv_file_length()
          
          with open(f'{dst}', 'wb') as file:
              bytes_read = 0
              while bytes_read < self.file_length:
                  data, addr = self._recv_data()
                  bytes_read += len(data)
                  file.write(data)
        except socket.timeout:
          print('Connection timed out')
          if os.path.isfile(dst):
            os.remove(dst)
        finally:
          self.sock.close()

    def ensure_send(self, data):
        start = datetime.now()
        if(self.sequence_number > 9999999999):
            self.sequence_number=1
        self._send_data(data)
        while(not self._recv_server_response()):
            now = datetime.now()
            if (now - start).seconds >= CONNECTION_TIMEOUT_SECS:
              raise socket.timeout
            self._send_data(data)
        self.sequence_number +=1


    def _recv_server_response(self):
        try:
            signal, addr = self.sock.recvfrom(CHUNK_SIZE)
            while int(signal.decode()) != self.sequence_number:
                signal, addr = self.sock.recvfrom(CHUNK_SIZE)
            return True
        except (socket.timeout):
            return False
   
    def _send_data(self,data):
       
        data = str(self.sequence_number).zfill(10).encode() + data
        self.sock.sendto(data,self.server_address)

    def _send_file_name(self,file_name):
        name_length = struct.pack('<I', len(file_name))
        self.ensure_send(name_length)
        self.ensure_send(file_name.encode())
    
    def _send_operation_mode_download(self):
        self.ensure_send(b'DNLD')

    def _recv_data(self):
        while True:
            data, addr = self.sock.recvfrom(CHUNK_SIZE)
            sequence_number = data[:10]
            data = data[10:]
            self.sock.sendto(sequence_number, addr)

            if (int(sequence_number) == (self.last_sequence_number + 1)):
                self.last_sequence_number +=1
                return data, addr
    
    def _recv_file_length(self):
        data, addr = self._recv_data()
        file_length, = struct.unpack('<I', data)
        return file_length