import socket
import os
import struct
from tcp_utils.send import send_data, send_file_name, send_operating_mode
from tcp_utils.constants import OP_CODE_UPLOAD, CHUNK_SIZE, METADATA_LENGTH_SIZE
from tcp_utils.connection_closed_error import ConnectionClosedError

def upload_file(server_address, src, name):
  if not os.path.isfile(src):
    print(f"Error starting the server: {src} is not a file")
    return

  print('TCP: upload_file({}, {}, {})'.format(server_address, src, name))
  cli = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

  try: 
    cli.connect(server_address)

    send_operating_mode(cli, OP_CODE_UPLOAD)
    send_file_name(cli, name)
    send_file_length(cli, src)

    with open(src, 'rb') as file:
      data = file.read(CHUNK_SIZE)
      while data != b'':
        send_data(cli, data, len(data))
        data = file.read(CHUNK_SIZE)
  except ConnectionRefusedError:
    print(f'Could not connect to server with address {server_address}')
  except ConnectionClosedError:
    print('The server connection was closed unexpectedly')
  finally:
    cli.close()

def send_file_length(conn, src):
  file_length = os.path.getsize(src)
  length = struct.pack('<I', file_length)
  send_data(conn, length, METADATA_LENGTH_SIZE)

