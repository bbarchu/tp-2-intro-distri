import socket
import os
from tcp_utils.send import send_data, send_file_name, send_operating_mode
from tcp_utils.receive import recv_data, recv_file_length
from tcp_utils.constants import OP_CODE_DOWNLOAD, SERVER_CONFIRMATION_LENGTH, CHUNK_SIZE
from tcp_utils.connection_closed_error import ConnectionClosedError

def download_file(server_address, name, dst):
  print('TCP: download_file({}, {}, {})'.format(server_address, name, dst))
  cli = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

  try: 
    cli.connect(server_address)

    send_operating_mode(cli, OP_CODE_DOWNLOAD)
    send_file_name(cli, name)
    response = recv_data(cli, SERVER_CONFIRMATION_LENGTH)
    if response == b'ERR':
      print('File does not exist on the server')
      cli.close()
      return

    file_length = recv_file_length(cli)
    
    try:
        with open(f'{dst}', 'wb') as file:
          bytes_read = 0
          while bytes_read < file_length:
            data = recv_data(cli, file_length - bytes_read, CHUNK_SIZE)
            bytes_read += len(data)
            file.write(data)
    except IOError:
      print(f'Could not write to file {dst}')

  except ConnectionRefusedError:
    print(f'Could not connect to server with address {server_address}')
  except ConnectionClosedError:
    print('The server connection was closed unexpectedly')
    if os.path.isfile(dst):
      os.remove(dst)
  finally:
    cli.close()

